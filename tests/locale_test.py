import builtins
from gettext import GNUTranslations
import os.path as path
from unittest import TestCase
from unittest.mock import patch
from tkml import locale

fixtures = path.realpath(path.join(path.dirname(__file__), 'fixtures'))
localedir = path.join(fixtures, 'locales')


class LocaleUseTest(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.lang = locale.use('locale_test', localedir=localedir,
                              language='en_US')

    def test_return_translator(self):
        self.assertIsInstance(self.lang, GNUTranslations)

    def test_install(self):
        self.assertEqual(self.lang.gettext, builtins._)
