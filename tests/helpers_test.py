from argparse import Namespace
import inspect
import os.path as path
from unittest import TestCase
import tkinter as tk
from tkinter import ttk
from tkinter.scrolledtext import ScrolledText
from tkml import helpers, locale

fixtures = path.realpath(path.join(path.dirname(__file__), 'fixtures'))
fixture = path.join(fixtures, 'enia-sample.yaml')
locale.use('loader_test', localedir=path.join(fixtures, 'locales'),
           language='en_US')


class FixturesTest(TestCase):

    def test_fixture_exposed(self):
        from tkml import fixtures as fixtures_
        self.assertIs(fixtures_, helpers.fixtures)

    def test_load_from_default_fixtures(self):
        self.verify_loader(helpers.fixtures())

    def test_load_from_supplied_directory(self):
        self.verify_loader(helpers.fixtures(fixtures))

    def verify_loader(self, loader):
        context = Namespace(
            search=lambda evt: None,
            set_entry=lambda widget: None,
            set_text=lambda widget: None,
        )

        root = loader('enia-sample.yaml', context=context)
        try:
            self.assertIsInstance(root, tk.Tk)
            top = root.nametowidget('!frame')
            self.assertEqual(['!frame', '!button', '!frame2'],
                             list(top.children.keys()))

        finally:
            root.quit()



class GetBackFrameTest(TestCase):
    def test_return_back_frame(self):
        self.assertIs(self.other_frame(), inspect.currentframe())

    def test_return_back_frame_for_inner_functions(self):
        def function():
            return helpers.get_back_frame()
        self.assertIs(function(), inspect.currentframe())

    def test_return_back_frame_for_outer_functions(self):
        self.assertIs(_aux_other_frame(), inspect.currentframe())

    def other_frame(self):
        return helpers.get_back_frame()


def _aux_other_frame():
    return helpers.get_back_frame()
