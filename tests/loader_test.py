from types import ModuleType as Module
import os.path as path
from unittest import TestCase, skip
from unittest.mock import MagicMock, call, patch
import tkinter as tk
import tkinter.ttk as ttk
from tkinter.scrolledtext import ScrolledText
from tkml import loader, locale

fixtures = path.realpath(path.join(path.dirname(__file__), 'fixtures'))
fixture = path.join(fixtures, 'enia-sample.yaml')
locale.use(
    'loader_test', localedir=path.join(fixtures, 'locales'), language='en_US',
)


class LoadFpTest(TestCase):

    described = staticmethod(loader.load_fp)
    calls = {}
    bind_mock = MagicMock()
    focus_mock = MagicMock()
    option_add_mock = MagicMock()
    pack_mock = MagicMock()

    @classmethod
    def setUpClass(cls):
        widgets = cls.widgets = {}

        # W0612: used through locals
        def search(evt):
            widgets['called'] = True

        # W0612: used through locals
        def set_entry(widget):
            widgets['entry'] = widget

        # W0612: used through locals
        def set_text(widget):
            widgets['text'] = widget

        with patch('tkinter.Widget.pack', cls.pack_mock), \
             patch('tkinter.Tk.option_add', cls.option_add_mock), \
             patch('tkinter.Widget.focus', cls.focus_mock), \
             patch('tkinter.Widget.bind', cls.bind_mock), \
             open(fixture) as fp:
            cls.root = cls.described(fp)

        cls.calls['search'] = search
        cls.calls['set_entry'] = set_entry
        cls.calls['set_text'] = set_text

    @classmethod
    def tearDownClass(self):
        self.root.quit()

    def test_root(self):
        self.assertIsInstance(self.root, tk.Tk)

    def test_top_child_is_a_frame(self):
        root = self.root
        self.assertEqual(['!frame'], list(root.children.keys()))
        frame = root.children['!frame']
        self.assertIsInstance(frame, ttk.Frame)

    def test_top_frame_have_three_children(self):
        top = self.root.children['!frame']
        self.assertEqual(['!frame', '!button', '!frame2'],
                         list(top.children.keys()))
        frame1, button, frame2 = top.children.values()
        self.assertIsInstance(frame1, ttk.Frame)
        self.assertIsInstance(button, ttk.Button)
        self.assertIsInstance(frame2, tk.Frame)  # build by ScrolledText

    def test_first_frame_have_two_children(self):
        frame = self.root.nametowidget('!frame.!frame')
        self.assertEqual(['!label', '!entry'], list(frame.children.keys()))
        label, entry = frame.children.values()
        self.assertIsInstance(label, ttk.Label)
        self.assertIsInstance(entry, ttk.Entry)

    def test_label_text(self):
        label = self.root.nametowidget('!frame.!frame.!label')
        self.assertEqual('Word to search:', label.cget('text'))

    def test_label_be_packed(self):
        # TODO: assert this call belongs to this label
        self.assertIn(call(anchor=tk.NW, side=tk.LEFT, expand=False),
                      self.pack_mock.call_args_list)

    def test_entry_be_packed(self):
        # TODO: assert this call belongs to this entry
        self.assertIn(call(anchor=tk.NE, side=tk.RIGHT, fill=tk.X, expand=True),
                      self.pack_mock.call_args_list)

    def test_entry_be_bound(self):
        # TODO: assert this call belongs to this entry
        self.assertIn(call('<Return>', self.calls['search']),
                      self.bind_mock.call_args_list)

    def test_entry_be_returned_by_name(self):
        entry = self.root.nametowidget('!frame.!frame.!entry')
        assert isinstance(entry, ttk.Entry)
        self.assertIs(entry, self.widgets['entry'])

    def test_entry_get_focus(self):
        # TODO: assert this call belongs to this entry
        self.focus_mock.assert_called_once_with()

    def test_first_frame_be_packed(self):
        # TODO: assert this call belongs to this frame
        self.assertIn(call(anchor=tk.N, fill=tk.X, expand=True),
                      self.pack_mock.call_args_list)

    def test_button_text(self):
        button = self.root.nametowidget('!frame.!button')
        self.assertEqual('Search', button.cget('text'))

    def test_button_command(self):
        widgets = self.widgets
        button = self.root.nametowidget('!frame.!button')
        assert not widgets.get('called')
        self.root.tk.call(button.cget('command'), '')  # button.invoke()
        self.assertTrue(widgets.get('called'))

    def test_button_be_packed(self):
        # TODO: assert this call belongs to this button
        self.assertIn(call(anchor=tk.S, expand=False),
                      self.pack_mock.call_args_list)

    def test_scrolled_text(self):
        frame = self.root.nametowidget('!frame.!frame2')
        self.assertEqual(['!scrollbar', '!scrolledtext'],
                         list(frame.children.keys()))
        self.assertIsInstance(frame.children['!scrollbar'], tk.Scrollbar)
        self.assertIsInstance(frame.children['!scrolledtext'], tk.Text)

    def test_scrolled_text_returned_by_self(self):
        text = self.root.nametowidget('!frame.!frame2.!scrolledtext')
        self.assertIs(self.widgets['text'], text)

    @skip('how to test it?')
    def test_scrolled_text_wrap(self):
        raise NotImplementedError

    def test_scrolled_text_be_packed(self):
        # TODO: assert this call belongs to this scrolled text
        self.assertIn(call(anchor=tk.S, fill=tk.BOTH, expand=True),
                      self.pack_mock.call_args_list)

    def test_top_frame_be_packed(self):
        # TODO: assert this call belongs to this frame
        self.assertIn(call(fill=tk.BOTH, expand=True),
                      self.pack_mock.call_args_list)

    def test_option_add_called_for_root(self):
        # TODO: assert this call belongs to root
        self.option_add_mock.assert_called_once_with('*tearOff', False)


class GetClassTest(TestCase):

    maxDiff = None
    described = staticmethod(loader.get_class)

    class X(Module):
        class A:
            pass
        class B:
            pass
        C = 'not class'

    class Y(Module):
        class B:
            pass
        class C:
            pass
        D = 'not class'

    def test_get_class_from_first_element(self):
        klass = self.described([self.X, self.Y], 'B')
        self.assertIs(klass, self.X.B)

    def test_get_class_from_first_found(self):
        klass = self.described([self.X, self.Y], 'C')
        self.assertIs(klass, self.Y.C)

    def test_not_found(self):
        with self.assertRaises(NameError):
            self.described([self.X, self.Y], 'D')


class ProcessValuesTest(TestCase):

    described = staticmethod(loader.process_values)

    def test_return_int_itself(self):
        self.assertEqual(self.described(1), 1)

    def test_return_object_itself(self):
        value = object()
        self.assertIs(self.described(value), value)

    def test_raise_key_error_on_unexistent_key(self):
        with self.assertRaises(KeyError):
            self.described('ctx[x]')

    def test_return_value_for_existent_key(self):
        self.assertEqual(self.described('ctx[x]', context={'x': 12}), 12)

    def test_return_string_when_not_key(self):
        self.assertEqual(self.described('x', context={'x': 12}), 'x')

    def test_parse_list(self):
        self.assertEqual(
            self.described(
                [42, 'ctx[x]', 'y'],
                context={'x': 12},
            ),
            [42, 12, 'y'],
        )

    def test_parse_dict(self):
        self.assertEqual(
            self.described(
                {'x': 'x', 'y': 23, 'name': 'ctx[name]'},
                context={'name': Ellipsis},
            ),
            {'x': 'x', 'y': 23, 'name': Ellipsis},
        )
